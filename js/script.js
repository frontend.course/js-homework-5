let number = null;

function isNumber(number) {
    return !isNaN(parseInt(number)) && isFinite(number) && number !== null && number !== '';
}

while (!isNumber(number)) {
    number = prompt('Введіть число');
}

number = parseInt(number);

const factorial = (number) => {
    if (number === 0) {
        return 1;
    }
    return number * factorial(number - 1);
}

console.log(factorial(number));

// Task 2

let fibonacciIndex = null;
while (!isNumber(fibonacciIndex)) {
    fibonacciIndex = prompt('Введіть число');
}

fibonacciIndex = parseInt(fibonacciIndex);

const fibonacci = (f1 = 0, f2 = 1, fibonacciIndex = 0) => {
    if (fibonacciIndex === 0) {
        return f1;
    }

    if (fibonacciIndex === 1) {
        return f2;
    }

    console.log(f2);
    return fibonacci(f2, f1 + f2, fibonacciIndex - 1);
}

console.log(fibonacci(0, 1, fibonacciIndex));